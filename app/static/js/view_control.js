// Интерфейс

var isInputWindowOpened = false;
var isHelpOpened = false;

var showHelpWindow = function() {
	var height = (isHelpOpened) ? '0px' : '200px';
	var opacity = (isHelpOpened) ? 0 : 1;
	var btnOpacity = (isHelpOpened) ? 1 : 0.5;

	$('.helpButton').css('opacity', btnOpacity);
    $('.help').css('height', height);
	$('.help').css('opacity', opacity);

	isHelpOpened = !isHelpOpened;
}

var showAccidentsCount = function() {
	var display = (isAccidentsCountOpened) ? 'none' : 'block';
	$('.accidents').css('display', display);
	isAccidentsCountOpened = !isAccidentsCountOpened;
}

var switchFormsVisiblity = function() {
	if ($('.algorithmForm').css('display') === 'none') {
		$('.inputForm').css('display', 'none');
		$('.algorithmForm').css('display', 'block');
	}
	else {
		$('.inputForm').css('display', 'block');
		$('.algorithmForm').css('display', 'none');
	}
}

function makeResultsTabs(clustersCount, itersCount) {
	$('.algoRes .clustersList').html('');
	$('.algoRes .allDistances tr.iterRow').remove();
	for (var i = 0; i < clustersCount; i++) {
		var elem = $('<li><a></a></li>');
		elem.find('a').html(i+1);
		elem.attr('data-id', i+1);
		$('.algoRes .clustersList').append(elem);
	}
}

document.querySelector('.algoRes').addEventListener('click', function(e) {
	$target = $(e.target);
	if ($target.is('.clustersList li a') || $target.is('.clustersList li')) {
		$('.algoRes .clustersList li').removeClass('active');
		$('.algoRes .allDistances tr.iterRow').removeClass('active');
		var $li = $target.closest('li');
		$li.addClass('active');
		routesLayer.clearLayers();
		routeLinesLayer.clearLayers();
		markersLayer.clearLayers();
		drawCluster(clusters[$li.attr('data-id')-1], colors[$li.attr('data-id')-1]);
		changeResultDisplayed();
	}
	else if ($target.is('.allDistances tr.iterRow') || $target.is('.allDistances tr.iterRow td')) {
		$('.algoRes .allDistances tr.iterRow').removeClass('active');
		var $tr = $target.closest('tr.iterRow');
		$tr.addClass('active');
		//routeLinesLayer.clearLayers();
		//markersLayer.clearLayers();
		var curId = $tr.attr('data-id');
		var clusterId = $('.algoRes .clustersList li.active').attr('data-id') - 1;
		drawClusterRoute(clusters[clusterId], colors[clusterId], results[clusterId][curId]["route"]);
	}
});

//changes list of iterations to be displayed
function changeResultDisplayed() {
	var curId = $('.algoRes li.active').attr('data-id');
	var curCluster = results[curId-1];
	$('.allDistances tr.iterRow').remove();
	for (var i = 0; i < curCluster.length; i++) {
		var elem = $('<tr class="iterRow clickable-row"></tr>');
		elem.attr('data-id', i);
		elem.html('<td class="number">' + curCluster[i]['iteration'] + '</td>' +
					'<td class="distance">' + Math.round(curCluster[i]['length']) + '</td>' +
					'<td class="time">' + getTimeString(curCluster[i]['time']) + '</td>');
		$('.algoRes .allDistances').append(elem);
	}
}

$('.helpButton').click(showHelpWindow);
$('.close').click(showHelpWindow);
$('.accidentsCheckbox').click(showAccidentsCount);

$('.backBtn').on('click', function(e) {
	e.preventDefault();
	switchFormsVisiblity();
	$('.algoRes').css('display', 'none');
	drawFullCluster([]);
})


// DRAWING

//Отрисовка всех кластеров
function drawFullCluster(clusters) {
	routesLayer.clearLayers();
	routeLinesLayer.clearLayers();
	markersLayer.clearLayers();
	for (var i = 0; i < clusters.length; i++) {
		drawCluster(clusters[i], colors[i]);
	}
}

//Отрисовка линий-"дорог", соединяющих точки в кластере
function drawCluster(cluster, color) {
	for (var i = 0; i < cluster.length; i++) {
		markersLayer.addLayer(L.marker(cluster[i], { title: i.toString() }));
		for (var j = i; j < cluster.length; j++) {
			routeLinesLayer.addLayer(L.polyline(
									[L.latLng(cluster[i]["lat"], cluster[i]["lng"]), 
									L.latLng(cluster[j]["lat"], cluster[j]["lng"])], 
									{ color: color }));
		}
	}
}

function drawClusterRoute(cluster, color, route) {
	routesLayer.clearLayers();
	markersLayer.clearLayers();
	markersLayer.addLayer(L.marker(cluster[route[0]], { title: "1" }));
	for (var i = 1; i < route.length; i++) {
		markersLayer.addLayer(L.marker(cluster[route[i]], { title: (i+1).toString() }));
		routesLayer.addLayer(L.polyline(
								[L.latLng(cluster[route[i]]["lat"], cluster[route[i]]["lng"]), 
								L.latLng(cluster[route[i-1]]["lat"], cluster[route[i-1]]["lng"])], 
								{ color: "#f00" }));
	}
}

function switchMessage() {
	var $message = $('.algorithmAlert');
	var display = ($message.css('display') == 'block') ? 'none' : 'block';
	$message.css('display', display);
}

function getRandomColor() {
	var color = "#";
	for (var i = 0; i < 6; i++) {
		var number = Math.round(Math.random() * 15);
		color += number.toString(16);
	}
	return color;
}

function getTimeString(time) {
	var workTime = $('input[name="workTime"]').val();
	var days = Math.ceil(time / workTime);
	var hours = Math.ceil(time % workTime);
	var minutes = Math.ceil((time % 1) * 60);
	var timeString = "";
	if (days > 0) {
		timeString += (days + " дн. ");
	}
	if (hours > 0) {
		timeString += (hours + " час. ");
	}
	if (minutes > 0) {
		timeString += (minutes + " мин.");
	}
	return timeString;
}
