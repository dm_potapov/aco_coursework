import random

class Ant(object):

    def __init__(self, acoInstance):
        self.route = []
        self.routeLength = 0
        self.instance = acoInstance
        print("hello i am ant (actually not)")

    def doRoute(self):
        curVertex = random.randint(0, len(self.instance.weightsMatrix) - 1)
        self.route.append(curVertex)
        for i in range(1, len(self.instance.weightsMatrix)):
            nextVertex = self.calculateNextVertex(curVertex)
            self.route.append(nextVertex)
            self.routeLength += self.instance.weightsMatrix[curVertex][nextVertex]
            self.instance.pheromonesMatrix[curVertex][nextVertex] += \
                            (self.instance.q / self.instance.weightsMatrix[curVertex][nextVertex])
            curVertex = nextVertex

    def calculateNextVertex(self, i):
        curProb = 0
        curIndex = 0
        for j in range(0, len(self.instance.weightsMatrix)):
            if j != i and j not in self.route:
                if self.prob(i, j) > curProb:
                    curProb = self.prob(i, j)
                    curIndex = j
        return curIndex

    def prob(self, i, j):
        numerator = (self.instance.pheromonesMatrix[i][j] ** self.instance.alpha) * \
                    ((1 / self.instance.weightsMatrix[i][j]) ** self.instance.alpha)
        denominator = 0
        for x in range(len(self.instance.weightsMatrix)):
            if x != j and x != i:
                denominator += ((self.instance.pheromonesMatrix[i][x] ** self.instance.alpha) * \
                                ((1 / self.instance.weightsMatrix[i][x]) ** self.instance.alpha))
        return numerator / denominator




