# coding=utf-8

from flask import Flask, render_template, request
from classes.aco_algorithm import *
from classes.cluster_builder import *
import os, json

app = Flask(__name__)

@app.route("/")
@app.route("/index/")
def index():
    return render_template("index.html")

@app.route("/sendData/", methods=['POST'])
def getData():
    #doAccidentsExist = request.form['accidents'] == 'on'
    points = json.loads(request.form['points'])
    builder = ClusterBuilder(points, int(request.form['bCount']))
    clusters = builder.getClusters()
    return str(clusters).replace("'", '"')

@app.route("/doAlgorithm/", methods=['POST'])
def doAlgorithm():
    print(request.form['persistance'])
    clusters = json.loads(request.form['clusters'])
    serviceTime = int(request.form['serviceTime'])
    workTime = int(request.form['workTime'])
    alpha = int(request.form['alpha'])
    beta = int(request.form['beta'])
    ants = int(request.form['antsCount'])
    persistance = float(request.form['persistance'])
    iterations = int(request.form['iterations'])
    results = []
    for i in clusters:
    	instance = ACOAlgorithm(i, ants, alpha, beta, persistance, iterations, serviceTime)
    	resForCluster = instance.doAlgorithm()
    	results.append(resForCluster)
    return str(results).replace("'", '"')


if __name__ == "__main__":
	app.run(debug=True)
