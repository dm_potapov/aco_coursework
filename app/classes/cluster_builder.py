#!coding=utf-8
import math, random, os

class ClusterBuilder(object):
    def __init__(self, points, count):
        self.points = points
        self.count = count

    def getClusters(self):
        points = sorted(self.points, key=lambda point: point['lat'] * point['lng'])
        clusters = [[] for i in range(self.count)]
        clustersSizes = self.getClustersSizesArray()
        offset = 0
        for i in range(len(clusters)):
            for j in range(clustersSizes[i]):
                clusters[i].append(points[j+offset])
            offset += clustersSizes[i]
        return clusters

    def getClustersSizesArray(self):
        pointsCount = len(self.points)
        clustersCount = self.count
        sizesArray = [math.floor(pointsCount / clustersCount) for i in range(clustersCount)]
        rest = pointsCount % clustersCount
        i = 0
        while i < clustersCount and rest != 0:
            sizesArray[i] += 1
            rest -= 1
            i+=1
        return sizesArray
