#!coding=utf-8
from .cluster_builder import *
from .ant import *

class ACOAlgorithm(object):
    # Инициализация константных параметров
    antsCount = 5 # Количество муравьёв в каждом кластере
    alpha = 2
    beta = 9
    q = 50 # Оптимальное расстояние между вершинами
    pheromonePersistence = 0.8 # Коэффициент испарения феромонов
    iterationsCount = 20

    def __init__(self, points, antsCount=5, alpha=2, beta=9, 
                    persistence=0.8, iters=20, serviceTime=1):
        self.antsCount = antsCount
        self.alpha = alpha
        self.beta = beta
        self.pheromonePersistence = persistence
        self.iterationsCount = iters
        self.serviceTime = serviceTime
        self.initWeightsMatrix(points)
        self.pheromonesMatrix = [[1.0 for j in range(len(points))] for i in range(len(points))]
        self.finalRoute = []
        self.finalRouteLength = 0
        for i in range(len(points)):
            self.finalRouteLength += self.weightsMatrix[0][i] 

    def initWeightsMatrix(self, points):
        self.weightsMatrix = []
        for i in range(len(points)):
            self.weightsMatrix.append([])
            for j in range(len(points)):
                weight = self.makeWeight(points[i], points[j])
                self.weightsMatrix[i].append(weight)

    def doAlgorithm(self):
        results = []
        for i in range(self.iterationsCount):
            antsPool = [Ant(self) for j in range(self.antsCount)]
            self.iteration(antsPool)
            detourTime = self.getDetourTime(self.finalRoute, self.finalRouteLength)
            print(str.format("route length {0} route: {1} time: {2}", self.finalRoute, self.finalRouteLength, detourTime))
            results.append({
                            "iteration" : i+1, 
                            "route":self.finalRoute, 
                            "length":self.finalRouteLength,
                            "time":detourTime
                        })
            for x in range(len(self.pheromonesMatrix)):
                for y in range(len(self.pheromonesMatrix)):
                    self.pheromonesMatrix[x][y] *= (1 - self.pheromonePersistence)
        return results

    def iteration(self, antsPool):
        for i in antsPool:
            i.doRoute()
            if (i.routeLength < self.finalRouteLength):
                self.finalRouteLength = i.routeLength
                self.finalRoute = i.route

    def getDetourTime(self, route, routeLength):
        return len(route) * self.serviceTime + routeLength / 60

    # Вычисление веса ребра
    # В данном случае вес - расстояние между точками, вычисленное по их координатам
    def makeWeight(self, pointOne, pointTwo):
        xDiff = (pointTwo["lat"] - pointOne["lat"]) ** 2
        yDiff = (pointTwo["lng"] - pointOne["lng"]) ** 2
        s = math.fabs(((xDiff + yDiff) ** 0.5) * 111)
        return s


if __name__ == "__main__":
    points = []
    for i in range(20):
        lat = random.random() * (49 - 46) + 46
        lng = random.random() * (48 - 39) + 39
        points.append({"lat": lat, "lng": lng})
    builder = ClusterBuilder(points, 3)
    clusters = [print(i) for i in builder.getClusters()]
    aco = ACOAlgorithm(points)
    allRes = aco.doAlgorithm()
    print(allRes)
