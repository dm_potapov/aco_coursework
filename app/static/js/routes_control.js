// Методы и прочее для ajax-запросов, получения ответов с сервера и отрисовки маршрутов


//Инициализация и отрисовка исходной карты
var map = L.map('map').setView([47, 43], 7);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(map);

var markersLayer = L.layerGroup().addTo(map); // Слой маркеров, обозначающих точки обслуживания
var routeLinesLayer = L.layerGroup().addTo(map); // Слой линий-"дорог"
var routesLayer = L.layerGroup().addTo(map); // Слой собственно маршрутов

var clusters = [];
var results = [];
var colors = [];

//TODO: доделать проверку входных данных
//Отправка на сервер входных данных
$('.inputForm').submit(function (event) {
	event.preventDefault();
	if (checkData()) {
		var points = generatePoints();
		var data = $('.inputForm').serializeArray();
		data[data.length] = { name: "points",  value: JSON.stringify(points) };
		$.ajax({
			url: '/sendData/',
			type: 'POST',
			data: data, 
			success: function (result) {
				clusters = JSON.parse(result);
				for (i in clusters) {
					colors[i] = getRandomColor();
				}
				drawFullCluster(clusters);
				switchFormsVisiblity();
			}
		});
	}
	else {
		alert("На одну бригаду должно приходиться по меньшей мере 5 объектов!");
	}
})

$('.algorithmForm').submit(function(e) {
	e.preventDefault();
	var data = $('.algorithmForm').serializeArray();
	data[data.length] = { name: 'clusters', value: JSON.stringify(clusters) };
	console.log(data);
	switchMessage();
	$.ajax({
		url: '/doAlgorithm/',
		method: 'POST',
		data: data,
		success: function(result) {
			console.log(result);
			results = JSON.parse(result);
			$('.algoRes').css('display', 'block');
			drawFullCluster(clusters);
			makeResultsTabs(results.length, results[0].length);
			switchMessage();
		}
	})
})

//Проверка входных данных
function checkData()  {
	var $form = $('.inputForm');
	if (($form.find('[name=aCount]').val() / 5 ) < 
		($form.find('[name=bCount]').val())) {
		return false;
	}
	else return true;
}

//Генерация точек - предполагаемых "подстанций"
function generatePoints() {
	var pointsArr = [];
	var objectsCount = $('[name=aCount]').val();
	for (var i = 0; i < objectsCount; i++) {
		var lat = Math.random() * (49 - 46) + 46,
			lng = Math.random() * (48 - 39) + 39
		pointsArr.push(L.latLng(lat, lng));
	}
	return pointsArr;
}
